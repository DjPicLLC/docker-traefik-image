#!/bin/bash

traefik_version=$1

docker build --build-arg traefik_version=$traefik_version --tag $CI_REGISTRY_IMAGE/traefik:$traefik_version --tag djpic/traefik:$traefik_version .

docker push $CI_REGISTRY_IMAGE/traefik:$traefik_version
docker push djpic/traefik:$traefik_version