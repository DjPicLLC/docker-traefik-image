#!/bin/bash

traefik_version=$1

docker build --build-arg traefik_version=$traefik_version --tag $CI_REGISTRY_IMAGE/traefik:$traefik_version-swarm --tag djpic/traefik:$traefik_version-swarm .

docker push $CI_REGISTRY_IMAGE/traefik:$traefik_version-swarm
docker push djpic/traefik:$traefik_version-swarm